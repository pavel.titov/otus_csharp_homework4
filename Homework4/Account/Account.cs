﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework4
{
    public class Account
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }

        public override string ToString()
        {
            return $"{LastName} {FirstName} {BirthDate.ToShortDateString()}";
        }
    }
}
