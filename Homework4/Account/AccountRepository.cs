﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Homework4
{
    public class AccountRepository : IRepository<Account>
    {
        private readonly ISerializer<Account> serializer;
        private readonly string filePath;

        public AccountRepository(ISerializer<Account> serializer, string filePath)
        {
            this.serializer = serializer;
            this.filePath = filePath;
        }

        public void Add(Account item)
        {
            var accounts = GetAll().Append(item);
            File.WriteAllText(filePath, serializer.Serialize(accounts.ToArray()));
        }

        public IEnumerable<Account> GetAll()
        {
            if (!File.Exists(filePath))
            {
                yield break;
            }

            using Stream stream = File.OpenRead(filePath);
            using var streamReader = new OtusStreamReader<Account>(stream, serializer);

            foreach (var person in streamReader)
            {
                yield return person;
            }
        }

        public Account GetOne(Func<Account, bool> predicate)
        {
            return GetAll().First(predicate);
        }
    }
}
