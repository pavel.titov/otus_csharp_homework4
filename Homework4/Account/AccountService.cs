﻿using System;

namespace Homework4
{
    public class AccountService : IAccountService
    {
        private readonly IRepository<Account> repository;

        public AccountService(IRepository<Account> repository)
        {
            this.repository = repository;
        }

        public void AddAccount(Account account)
        {
            if (String.IsNullOrEmpty(account.FirstName))
            {
                throw new ArgumentException("First name must not be null or empty");
            }
            if (String.IsNullOrEmpty(account.LastName))
            {
                throw new ArgumentException("Last name must not be null or empty");
            }
            if (account.BirthDate.AddYears(18) > DateTime.Now.Date)
            {
                throw new ArgumentException("Age must be > 18");
            }

            repository.Add(account);
        }
    }
}
