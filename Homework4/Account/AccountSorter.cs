﻿using System.Collections.Generic;
using System.Linq;

namespace Homework4
{
    public class AccountSorter : IAlgorithm<Account>
    {
        public IEnumerable<Account> Sort(IEnumerable<Account> notSortedItems)
        {
            return notSortedItems.OrderBy(p => p.FirstName);
        }
    }
}
