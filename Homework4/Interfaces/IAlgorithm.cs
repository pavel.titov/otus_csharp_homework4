﻿using System.Collections.Generic;

namespace Homework4
{
    public interface IAlgorithm<T>
    {
        IEnumerable<T> Sort(IEnumerable<T> notSortedItems);
    }
}
