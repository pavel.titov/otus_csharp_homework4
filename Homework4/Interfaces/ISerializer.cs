﻿using System.IO;

namespace Homework4
{
    public interface ISerializer<T>
    {
        string Serialize(T[] item);

        T[] Deserialize(Stream stream);
    }
}
