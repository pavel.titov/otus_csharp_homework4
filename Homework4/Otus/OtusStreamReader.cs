﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Homework4
{
    public class OtusStreamReader<T> : IEnumerable<T>, IDisposable
    {
        private readonly Stream stream;
        private readonly ISerializer<T> serializer;
        private bool disposedValue;

        public OtusStreamReader(Stream stream, ISerializer<T> serializer)
        {
            this.stream = stream;
            this.serializer = serializer;
        }

        public IEnumerator<T> GetEnumerator()
        {
            T[] array = serializer.Deserialize(stream);

            foreach (T i in array)
            {
                yield return i;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    stream.Dispose();
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
