﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System.IO;
using System.Xml;

namespace Homework4
{
    public class OtusXmlSerializer<T> : ISerializer<T>
    {
        private static readonly IExtendedXmlSerializer Serializer;
        private static readonly XmlWriterSettings XmlWriterSettings;

        static OtusXmlSerializer()
        {
            Serializer = new ConfigurationContainer().UseAutoFormatting()
                                                     .UseOptimizedNamespaces()
                                                     .EnableImplicitTyping(typeof(T))
                                                     .Create();
            XmlWriterSettings = new XmlWriterSettings() { Indent = true };
        }

        public T[] Deserialize(Stream stream)
        {
            using XmlReader reader = XmlReader.Create(stream);

            return (T[])Serializer.Deserialize(reader);
        }

        public string Serialize(T[] item)
        {
            return Serializer.Serialize(XmlWriterSettings, item);
        }
    }
}
