﻿using System;
using System.IO;

namespace Homework4
{
    class Program
    {
        static void Main(string[] args)
        {
            var serializer = new OtusXmlSerializer<Account>();

            string filePath = "accounts.xml";
            File.Delete(filePath);

            var accountRepository = new AccountRepository(serializer, filePath);
            var accountService = new AccountService(accountRepository);
            var accountSorter = new AccountSorter();

            accountService.AddAccount(new Account() { LastName = "Ivanov", FirstName = "Vasiliy", BirthDate = new DateTime(2000, 1, 1) });
            accountService.AddAccount(new Account() { LastName = "Petrov", FirstName = "Ivan", BirthDate = new DateTime(1995, 12, 31) });
            accountService.AddAccount(new Account() { LastName = "Sidorov", FirstName = "Petr", BirthDate = new DateTime(1991, 5, 1) });

            var accounts = accountSorter.Sort(accountRepository.GetAll());

            foreach (var account in accounts)
            {
                Console.WriteLine(account);
            }

            {
                var account = accountRepository.GetOne(a => a.LastName == "Sidorov");
                Console.WriteLine(account);
            }
        }
    }
}
