﻿using Homework4;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace XUnitTestProject
{
    public class TestAccountService
    {
        private readonly List<Account> accounts;
        private readonly AccountService accountService;

        public TestAccountService()
        {
            accounts = new List<Account>();

            var mock = new Mock<IRepository<Account>>();
            mock.Setup(x => x.Add(It.IsAny<Account>())).Callback<Account>(a => accounts.Add(a));

            accountService = new AccountService(mock.Object);
        }

        [Fact]
        public void AddValidAccounts()
        {
            var validAccount1 = new Account() { LastName = "Testov", FirstName = "Test", BirthDate = new DateTime(1999, 1, 1) };
            var validAccount2 = new Account() { LastName = "Testov", FirstName = "Test2", BirthDate = new DateTime(2000, 1, 1) };

            accountService.AddAccount(validAccount1);
            Assert.Single(accounts);
            Assert.Equal(validAccount1, accounts[0]);

            accountService.AddAccount(validAccount2);
            Assert.Equal(2, accounts.Count);
            Assert.Equal(validAccount2, accounts[1]);
        }

        [Fact]
        public void AddAccountWithInvalidLastName()
        {
            var invalidLastName = new Account() { FirstName = "Test", BirthDate = new DateTime(1999, 1, 1) };

            Assert.Throws<ArgumentException>(() => accountService.AddAccount(invalidLastName));
        }

        [Fact]
        public void AddAccountWithInvalidFirstName()
        {
            var invalidFirstName = new Account() { LastName = "Testov", FirstName = "", BirthDate = new DateTime(1999, 1, 1) };

            Assert.Throws<ArgumentException>(() => accountService.AddAccount(invalidFirstName));
        }

        [Fact]
        public void AddAccountWithInvalidBirthDate()
        {
            var invalidBirthDate = new Account() { LastName = "Testov", FirstName = "Test", BirthDate = DateTime.Now };

            Assert.Throws<ArgumentException>(() => accountService.AddAccount(invalidBirthDate));
        }
    }
}
